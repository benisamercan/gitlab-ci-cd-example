# Deployment Processes With Gitlab CI/CD

## About CI/CD Tools
<hr>

Hello everyone, I want to talk about ci / cd processes in this article, as you know, there are many ci / cd tools, the most used as examples are jenkins, teamcity, circleci, bamboo, gitlab etc .., and more ..


 <center><img src="./files/CICD_Tools.png"></center>


Until the applications we developed are broadcast live, we run certain commands, if we give an example to these;
Creating a package from the current version of the application on git
Publishing the package received in the test environment
Running test scenarios
Such as publishing an application to the production environment

Now that we have talked about the ci / cd processes, we can start to talk about the gitlab ci / cd process in this article.


## Gitlab
<hr>

From project planning and source code management to CI/CD and monitoring, GitLab is a complete DevOps platform, delivered as a single application.


## Applications

First of all, we need to create a repository on gitlab, as an example application, we have 2 applications that will output "hello world" in python, so we will see how we can deploy more than one application under one solution.

In this case my application names are application-one.py and application-two.py

```python
import os
print("Hello World")
print(os.environ.get('ENV'))
```


## CI / CD
<hr>

### Configuration of Repository Settings

In order to run the gitlab ci process, you must create the .gitlab-ci.yml file in the application and specify this file path in the CI / CD under settings in the repository in gitlab.


 <center><img src="./files/general-pipelines.png"></center>


In the image above, there is the setting where gitlab-ci.yml is defined.

Root .gitlab-ci.yml

``` yaml
stages:
  - "triggers"

Application One:
  stage: triggers
  trigger:
    include:
      local: app/one/gitlab-ci.yml
  only:
    changes:
      - "app/one/**/*"

Application Two:
  stage: triggers
  trigger:
    include:
      local: app/two/gitlab-ci.yml
  only:
    changes:
      - "app/two/**/*"
```

Example for application-one gitlab-ci.yml

``` yaml

variables:
  deployment_name: "application_one"
  application_number: "application one"

stages:
  - Sonarqube
  - Build
  - Stage
  - Test
  - Publish


Code:
  stage: Sonarqube
  tags:
    - gitlab-runner
  script:
    - echo "sonar qube scripts"
  timeout: 10 minutes

Package:
  stage: Build 
  needs: ["Code"]
  tags:
    - gitlab-runner
  script:
    - echo "build commands"
  when: manual
  retry:
    max: 2
    when: runner_system_failure
  timeout: 3 minutes

Deploy DEV:
  stage: Stage
  needs: ["Package"]
  tags:
    - gitlab-runner
  script:
    - export ENV=DEV
    - echo "deploy dev environment"
  timeout: 10 minutes
  when: manual
  environment:
    name: DEV/${deployment_name}
  allow_failure: false
  
Deploy TEST:
  stage: Stage
  needs: ["Package"]
  tags:
    - gitlab-runner
  script:
    - export ENV=test
    - echo "deploy test environment"
  timeout: 10 minutes
  when: manual
  environment:
    name: TEST/${deployment_name}
  allow_failure: false

Integration TESTS:
  stage: Test
  needs: ["Deploy TEST"]
  tags:
    - gitlab-runner
  script:
    - echo "run integration tests scripts"
  timeout: 10 minutes
  when: manual
  allow_failure: false

Deploy Production:
  stage: Publish
  needs: ["Integration TESTS"]
  tags:
    - gitlab-runner
  script:
    - export ENV=production
    - python3 app/one/${deployment_name}.py
  only:
    - master
  timeout: 10 minutes
  when: manual
  environment:
    name: Production/${deployment_name}
  allow_failure: false

```



### Gitlab Runner

Gitlab runner, which will run the commands in the .gitlab-ci.yml file we have prepared, we need to install it on a machine, all the operations will be executed by gitlab-runner, and the commands it will run will be written to the .gitlab-ci.yml file.

**You can restrict branch based deployments and make stages dependent on each other.**

**With Gitlab runner, you can run multiple pipelines at the same time.
We specify which runner will perform this step by typing the keyword in the tags field in Yaml file**

**In the environment where the runner is running, the files of the machine it is hosted are not directly accessed, the runner prepares and runs a virtual environment in the environment it finds. That's why you should put an example of the files you want to use on the host machine in the directory where the gitlab runner application is located. For example, like kubernetes config file**


### Installation

[Documentation](https://docs.gitlab.com/runner/install/) 

We will use linux here, other operating systems are available on the link I shared

Replace ${arch} with any of the supported architectures, e.g. amd64, arm, arm64
A full list of architectures can be found here https://gitlab-runner-downloads.s3.amazonaws.com/latest/index.html

We will use amd64 prefix

```curl
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
```

Output

 <center><img src="./files/gitlab_runner_install.jpg"></center>


We have set up gitlab runner, now we need to register, we need our token information in gitlab, click the Runners expand button in ci / cd under settings from the repository, and open the Runners tab.

We have token information here.

 <center><img src="./files/setup_instroduction.jpg"></center>

Execute command on shell

```# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permissions to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab CI user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start


sudo gitlab-runner register --url https://gitlab.com/ --registration-token YOUR_PROJECT_TOKEN
```

if you get an error from trigger a pipe like the following

```code
ERROR: Job failed (system failure): prepare environment: exit status 1. Check https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading for more information
```

use this command on runner machine
```code
sudo rm /home/gitlab-runner/.bash_logout
```

 <center><img src="./files/available_runner.png"></center>



 After all the steps are completed, the first commit will also create a runner pipeline. You can access this pipeline from "CI / CD -> Pipelines".


 <center><img src="./files/Pipeline.png"></center>



## About Global Environment Veriables

You can make project-based or group-based definitions in Variables under "CI / CD" Settings, you can read these assigned variables from gitlab-ci.yml. For example, you can keep docker registry address / username / password, application replica numbers based on the environment.
If you want to use a package based version number I recommend using "${CI_PIPELINE_ID}".

## About .gitlab-ci.yml

[Keyword reference for the .gitlab-ci.yml file](https://docs.gitlab.com/ee/ci/yaml/)

[Gitlab Project Link](https://gitlab.com/ferhatcandas/gitlab-ci-cd-example)

## Resources 

https://docs.gitlab.com/ee/ci/

https://docs.gitlab.com/ee/ci/pipelines/

https://docs.gitlab.com/ee/ci/multi_project_pipelines.html
